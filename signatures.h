bool verifAlphaNum(wchar_t* chaine, int longueur);
void modifierAccents(wchar_t* chaine, int longueur);
void cesar(wchar_t* chaine, int cle, int longueur);
void dechiffrerCesar(wchar_t* chaine, int cle, int longueur);
void viderBuffer();
void vigenere(wchar_t* chaine, wchar_t* cleV, int longueur);
void dechiffrerVigenere(wchar_t* chaine, wchar_t* cleV, int longueur);