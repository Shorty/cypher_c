#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wctype.h>
#include <stdbool.h>
#include <wchar.h>
#include <locale.h>
#include "signatures.h"
#define	TAILLE 100

wchar_t chaine[TAILLE] = {L'\0'};
wchar_t cleV[TAILLE] = {L'\0'};
wchar_t cleV2[TAILLE] = {L'\0'};
wchar_t accents[28] = {L'é', L'è', L'ê', L'ë', L'É', L'È', L'Ê', L'Ë', L'à', L'â', L'ä', L'À', L'Â', L'Ä', L'ù', L'û', L'ü', L'Ù', L'Û', L'Ü', L'ì', L'î', L'ï', L'Ì', L'Î', L'Ï', L'ç', L'Ç'};

int cle;
int cleverif;
bool verif;
int typeChiffrement;
int longueur = 0;

void main() {

	setlocale(LC_ALL, "");
	wprintf(L"Saisissez une chaine alphanumérique :\n");
	fgetws(chaine, TAILLE, stdin);
	//viderBuffer();
	wprintf(L"Quel type de chiffrement ? César 1 Vigenère 2 :\n");
	wscanf(L"%d", &typeChiffrement);

	while (chaine[longueur] != L'\n' && longueur < TAILLE) {
		longueur++;
	}

	verif = verifAlphaNum(chaine, longueur);
	if (typeChiffrement == 2) { //on vérifie que la chaine ne contient pas de chiffre pour le chiffrement vigenère
		for (int i = 0; i<longueur; i++) {
			if (chaine[i] <= 57) verif = false;
		}
	}
	modifierAccents(chaine, longueur);

	viderBuffer();
	if (verif) {
		if (typeChiffrement == 1) {

			wprintf(L"Saisissez la clé :\n");
			if (wscanf(L"%d", &cle) == 1) {
				cesar(chaine, cle, longueur);
				wprintf(L"Chaine chiffrée : %ls\n", chaine);
				dechiffrerCesar(chaine, cle, longueur);
				wprintf(L"Chaine déchiffrée : %ls\n", chaine);
			} else {
				wprintf(L"La clé est invalide, elle doit être un nombre.\n");
			}

		} else if (typeChiffrement == 2) {

			wprintf(L"Saisissez la clé :\n");
			viderBuffer();
			fgetws(cleV, TAILLE, stdin);
			fgetws(cleV, TAILLE, stdin);

			vigenere(chaine, cleV, longueur);
			wprintf(L"Chaine chiffrée : %ls\n", chaine);
			dechiffrerVigenere(chaine, cleV, longueur);
			wprintf(L"Chaine déchiffrée : %ls\n", chaine);

		} else {
			wprintf(L"Type de chiffrement invalide.\n");
		}
	} else {
		wprintf(L"Votre saisie est invalide.\n");
	}
	
}

bool verifAlphaNum(wchar_t* chaine, int longueur) {

	//setlocale(LC_ALL, "");
	bool isCorrect = true;
	wprintf(L"chaine saisie : %ls\n", chaine);
	for(int i=0; i < longueur; i++){ //compte le nombre de caractères saisis
		if(iswalnum(chaine[i]) == 0 && chaine[i] != L' '){ //si la chaîne n'est pas alphanumérique
			isCorrect = false;
		}
	}

	return isCorrect;

}

void modifierAccents(wchar_t* chaine, int longueur) {

	for(int i = 0; i < longueur; i++) {
		for(int j = 0; j < 28; j++) {
			if (chaine[i] == accents[j] && j <= 3) {
				chaine[i] = L'e';
			} else if (chaine[i] == accents[j] && j <= 7) {
				chaine[i] = L'E';
			} else if (chaine[i] == accents[j] && j <= 10) {
				chaine[i] = L'a';
			} else if (chaine[i] == accents[j] && j <= 13) {
				chaine[i] = L'A';
			} else if (chaine[i] == accents[j] && j <= 16) {
				chaine[i] = L'u';
			} else if (chaine[i] == accents[j] && j <= 19) {
				chaine[i] = L'U';
			} else if (chaine[i] == accents[j] && j <= 22) {
				chaine[i] = L'i';
			} else if (chaine[i] == accents[j] && j <= 25) {
				chaine[i] = L'I';
			} else if (chaine[i] == accents[j] && j == 26) {
				chaine[i] = L'c';
			} else if (chaine[i] == accents[j] && j == 27) {
				chaine[i] = L'C';
			}
		}
	}

}

void cesar(wchar_t* chaine, int cle, int longueur) { //chiffrement césar avec la table ascii

	int newcle;
	for(int i = 0; i<longueur; i++) {
		if (chaine[i] != L' ' && chaine[i] <= 57) { //si le caractère est un chiffre
			newcle = (chaine[i]-47 + (cle%10)) % 10;
			if (newcle <= 0) { //en cas de clé négative
				newcle = newcle+10;
			}
			chaine[i] = 47 + newcle;
		} else if (chaine[i] != L' ' && chaine[i] <= 90) { //si le caractère est une majuscule
			newcle = (chaine[i]-64 + (cle%26)) % 26;
			if (newcle <= 0) {
				newcle = newcle+26;
			}
			chaine[i] = 64 + newcle;
		} else if (chaine[i] != L' ' && chaine[i] <= 122) { //si le caractère est une minuscule
			newcle = (chaine[i]-96 + (cle%26)) % 26;
			if (newcle <= 0) {
				newcle = newcle+26;
			}
			chaine[i] = 96 + newcle;
		}
	}

}

void dechiffrerCesar(wchar_t* chaine, int cle, int longueur) {

	cle = 0-cle; //Notre fonction cesar est capable de gérer les clés négatives, donc on peut déchiffrer en prenant l'opposé de la clé
	cesar(chaine, cle, longueur);

}

void vigenere(wchar_t* chaine, wchar_t* cleV, int longueur) {

	int longueurCle = 0;
		while (cleV[longueurCle] != L'\n' && longueurCle < TAILLE) {
			longueurCle++;
	}

	int newcle;
	int k = 0;
	for (int i = 0; i<longueur; i++) { //on adapte la clé à la longueur de la chaine
		if (chaine[i] != L' ') {
			cleV2[i] = cleV[k];
			if (k < longueurCle-1) {
				k++;
			} else {
				k = 0;
			}
		} else {
			cleV2[i] = 48; //s'il y a un espace dans la chaine alors peu importe la valeur de la clé (ici '0')
		}
	}

	for(int i = 0; i<longueur; i++) {
		if (chaine[i] != L' ' && chaine[i] <= 90) { //si le caractère est une majuscule
			newcle = (chaine[i]-65 + towupper(cleV2[i])-65) % 26;
			chaine[i] = 65 + newcle;
		} else if (chaine[i] != L' ') { //le caractère est une minuscule
			newcle = (chaine[i]-97 + towlower(cleV2[i])-97) % 26;
			chaine[i] = 97 + newcle;
		}
	}

}

void dechiffrerVigenere(wchar_t* chaine, wchar_t* cleV, int longueur) {

	int longueurCle = 0;
		while (cleV[longueurCle] != L'\n' && longueurCle < TAILLE) {
			longueurCle++;
	}

	int newcle;
	int k = 0;
	for (int i = 0; i<longueur; i++) { //on prend l'opposé de la clé et on l'adapte à la longueur de la chaine
		if (chaine[i] != L' ') {
			if (chaine[i] <= 90) {
				cleV2[i] = 65-towupper(cleV[k]);
			} else {
				cleV2[i] = 97-towlower(cleV[k]);
			}
			if (k < longueurCle-1) {
				k++;
			} else {
				k = 0;
			}
		} else {
			cleV2[i] = 48; //s'il y a un espace dans la chaine alors peu importe la valeur de la clé (ici '0')
		}
	}

	for (int i = 0; i<longueur; i++) {
		if (chaine[i] != L' ' && chaine[i] <= 90) { //si le caractère est une majuscule
			newcle = chaine[i]-65+cleV2[i];
			if (newcle < 0) {
				newcle = newcle+26;
			}
			chaine[i] = 65 + newcle;
		} else if (chaine[i] != L' ') { //le caractère est une minuscule
			newcle = chaine[i]-97+cleV2[i];
			if (newcle < 0) {
				newcle = newcle+26;
			}
			chaine[i] = 97 + newcle;
		}
	}

}

void viderBuffer() {
    int c = 0;
    while (c != L'\n' && c != EOF) {
        c = getchar();
    }
}