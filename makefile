GCC = gcc
SOURCES = $(wildcard *.c)
BINAIRES = $(patsubst %.c,%.o,${SOURCES})


all: main

main: ${BINAIRES}
#${GCC} KIPFER_PRINCEAU_S2E_CHIFFREMENT.c -o main
	${GCC} $^ -o $@

#KIPFER_PRINCEAU_S2E_CHIFFREMENT.o: KIPFER_PRINCEAU_S2E_CHIFFREMENT.c 
%.o: %.c signatures.h
#${GCC} -c KIPFER_PRINCEAU_S2E_CHIFFREMENT.c
	${GCC} -c $<
	
clean:
	rm main
	rm *.o
