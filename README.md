Notre projet de chiffrement s'appelle Cypher_c et a été développé par Jimmy Kipfer et Matthieu Princeau.
Il contient 2 fichiers de code en C et un makefile et permet de chiffrer et déchiffrer un message entré par l'utilisateur selon les algortihmes de Cesar et / ou de Vigenère.
lien vers le dépôt Git : https://framagit.org/Shorty/cypher_c

Fonctions :

bool verifAlphaNum(wchar_t* chaine, int longueur);
Cette fonction permet de vérifier que le message saisi par l'utilisateur est bien alphanumérique.
entrée : message de l'utilisateur et un indice pour parcourir la chaîne donnant le nombre de caractères
sortie : boolean true si la chaîne est alphanumérique
gestion d’erreur : si un caractère n’est pas alphanumérique, affichage d’un message d’erreur.

void modifierAccents(wchar_t* chaine, int longueur);
Permet d’enlever les accents des caractères accentués afin de simplifier le chiffrement (et de respecter la consigne).
entrée : message de l'utilisateur et un indice pour parcourir la chaîne donnant le nombre de caractères
sortie : pas de sortie car fonction de modification
gestion d’erreur : aucune car tout est vérifié par verifAlphaNum

void cesar(wchar_t* chaine, int cle, int longueur);
Permet de chiffrer une phrase avec la méthode César
entrée : chaîne préalablement saisie et validée, clé saisie et validée aussi
sortie : chaîne chiffrée suivant la méthode César
erreurs : la clé doit être un nombre

void dechiffrerCesar(wchar_t* chaine, int cle, int longueur);
Permet de déchiffrer une phrase chiffrée avec la méthode César
entrée : chaîne chiffrée suivant la méthode César, clé saisie et validée
sortie : chaîne déchiffrée grâce à la clé

void viderBuffer();
Permet de vider le buffer afin de ne pas garder en mémoire des caractères étrangers à la chaîne analysée.
pas d’entrée ni de sortie ni de gestion d’erreur

void vigenere(wchar_t* chaine, wchar_t* cleV, int longueur);
Permet de chiffrer une phrase avec la méthode Vigenère
entrée : chaîne préalablement saisie et validée, clé saisie et validée aussi
sortie : chaîne chiffrée suivant la méthode Vigenère
erreurs : la clé doit uniquement contenir des lettres

void dechiffrerVigenere(wchar_t* chaine, wchar_t* cleV, int longueur);
Permet de déchiffrer un message codé selon l’algorithme de Vigenère
entrée : message de l’utilisateur, la clé utilisée pour chiffrer et un indice pour parcourir la chaîne donnant le nombre de caractères.
sortie : chaîne déchiffrée grâce à la clé
gestion d’erreur : none
